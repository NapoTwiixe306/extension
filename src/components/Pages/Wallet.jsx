import React, { useEffect, useState } from "react";
import {
  fetchBitcoinPrice,
  fetchBnbPrice,
  fetchEthPrice,
  fetchDotPrice,
  fetchAlgoPrice,
} from "../Js/test";
import Bitcoin from "../img/bitcoin.png";
import bnb from "../img/bnb.png";
import eth from "../img/eth.png";
import dot from "../img/dot.png";
import algo from "../img/algo.png";
const Wallet = () => {
  const [bitcoinPrice, setBitcoinPrice] = useState("");
  const [bnbPrice, setBnbPrice] = useState("");
  const [ethPrice, setEthPrice] = useState("");
  const [dotPrice, setDotPrice] = useState("");
  const [algoPrice, setAlgoPrice] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const price = await fetchBitcoinPrice();
      const bnbPrice = await fetchBnbPrice();
      const ethPrice = await fetchEthPrice();
      const dotPrice = await fetchDotPrice();
      const algoPrice = await fetchAlgoPrice();

      setBitcoinPrice(parseFloat(price).toFixed(4));
      setBnbPrice(parseFloat(bnbPrice).toFixed(4));
      setEthPrice(parseFloat(ethPrice).toFixed(4));
      setDotPrice(parseFloat(dotPrice).toFixed(4));
      setAlgoPrice(parseFloat(algoPrice).toFixed(4));
    };

    fetchData();
  }, []);

  return (
    <>
      <div>
        <div className="walletSection">
          <section>
            <img src={Bitcoin} alt="" />
            <p className="currency-name">Bitcoin</p>
            <p>{`$${bitcoinPrice}`}</p>
            <p>$Graph</p>
          </section>
          <section>
            <img src={bnb} alt="" />
            <p className="currency-name">Binance</p>
            <p>{`$${bnbPrice}`}</p>
            <p>$Graph</p>
          </section>
          <section>
            <img src={eth} alt="" />
            <p className="currency-name">Ethereum</p>
            <p>{`$${ethPrice}`}</p>
            <p>$Graph</p>
          </section>
          <section>
            <img src={dot} alt="" />
            <p className="currency-name">Polkadot</p>
            <p>{`$${dotPrice}`}</p>
            <p>$Graph</p>
          </section>
          <section>
            <img src={algo} alt="" />
            <p className="currency-name">AlgoLand</p>
            <p>{`$${algoPrice}`}</p>
            <p>$Graph</p>
          </section>
        </div>
      </div>
    </>
  );
};

export default Wallet;
