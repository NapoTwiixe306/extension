import React, { useState } from "react";
import { FaCog } from "react-icons/fa";
import { connectWallet, formatAddress } from "./Js/connect";

const Navbar = () => {
  const [address, setAddress] = useState(null);

  const handleConnect = async () => {
    try {
      const connectedAddress = await connectWallet();
      setAddress(connectedAddress);
    } catch (error) {
      console.error('Erreur lors de la connexion:', error);
    }
  };

  return (
    <div className="w-[300px] h-[400px] flex flex-col p-4 bg-gray-800">
      <header className="flex items-center justify-between mb-4">
        <button 
          onClick={handleConnect} 
          className="px-4 py-2 text-white bg-blue-500 rounded hover:bg-blue-600"
        >
          {address ? formatAddress(address) : 'Connecter'}
        </button>
        <FaCog className="w-5 h-5 text-white"/>
      </header>
      <div className="w-full h-[3px] bg-[linear-gradient(90deg,_rgba(255,_255,_255,_0.00)_0%,_rgba(54,_124,_255,_0.49)_50%,_rgba(255,_255,_255,_0.00)_100%)]"></div>
    </div>
  );
};

export default Navbar;
