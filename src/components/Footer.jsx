import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <p className="footer__title">CryptoExtension</p>
      <p className="copy">
        &#169; <span id="date"></span> copyright all right reserved
      </p>
    </footer>
  );
};

export default Footer;
