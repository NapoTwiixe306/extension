import React from 'react'
import Navbar from './Navbar'

export default function Home() {
  return (
    <>
      <div className='min-h-screen bg-gray-900'>
        <Navbar/>
        <div className='p-4'>
          <p className='text-white '>Title</p>
        </div>
      </div>
    </>
  )
}
