const fetchCryptoPrice = async (crypto) => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=${crypto}`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};

export default fetchCryptoPrice;
