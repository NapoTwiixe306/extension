import { Web3Provider } from '@ethersproject/providers';
import { ethers } from 'ethers';

// Fonction pour se connecter avec MetaMask
export const connectWallet = async () => {
  if (window.ethereum) {
    try {
      // Demande de connexion à MetaMask
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      const provider = new Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const address = await signer.getAddress();

      return address;
    } catch (error) {
      console.error('Erreur lors de la connexion avec MetaMask:', error);
      return null;
    }
  } else {
    console.error('MetaMask non détecté');
    return null;
  }
};

// Fonction pour afficher l'adresse partiellement masquée
export const formatAddress = (address) => {
  if (address) {
    return `${address.slice(0, 6)}...${address.slice(-4)}`;
  }
  return '';
};
