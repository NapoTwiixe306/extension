export const fetchBitcoinPrice = async () => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};

export const fetchBnbPrice = async () => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=BNBUSDT`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};

export const fetchEthPrice = async () => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};

export const fetchDotPrice = async () => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=DOTUSDT`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};

export const fetchAlgoPrice = async () => {
  let apiUrl = `https://api.binance.com/api/v3/ticker/price?symbol=ALGOUSDT`;
  let response = await fetch(apiUrl);
  let data = await response.json();
  return data.price;
};
