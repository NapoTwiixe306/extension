import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";

import { Home } from "./components";
import Wallet from "./components/Pages/Wallet";
import "./index.css";

function App() {
  return (
      <HashRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/wallet" element={<Wallet />} />
        </Routes>
      </HashRouter>
  );
}

export default App;
